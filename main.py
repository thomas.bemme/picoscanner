import rp2
import network
import ubinascii
import machine
import urequests as requests
import utime as time
from secrets import secrets
import socket

# Set country to avoid possible errors
rp2.country('DE')

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
# If you need to disable powersaving mode
# wlan.config(pm = 0xa11140)

# See the MAC address in the wireless chip OTP
mac = ubinascii.hexlify(network.WLAN().config('mac'),':').decode()
print('mac = ' + mac)

# Other things to query
print(wlan.config('channel'))
print(wlan.config('essid'))
print(wlan.config('txpower'))

# Load login data from different file for safety reasons
ssid = secrets['ssid']
pw = secrets['pw']

wlan.connect(ssid, pw)

# Wait for connection with 10 second timeout
timeout = 10
while timeout > 0:
    if wlan.status() < 0 or wlan.status() >= 3:
        break
    timeout -= 1
    print('Waiting for connection...')
    time.sleep(1)

# Define blinking function for onboard LED to indicate error codes    
def blink_onboard_led(num_blinks):
    led = machine.Pin('LED', machine.Pin.OUT)
    for i in range(num_blinks):
        led.on()
        time.sleep(.2)
        led.off()
        time.sleep(.2)
    
# Handle connection error
# Error meanings
# 0  Link Down
# 1  Link Join
# 2  Link NoIp
# 3  Link Up
# -1 Link Fail
# -2 Link NoNet
# -3 Link BadAuth

wlan_status = wlan.status()
blink_onboard_led(wlan_status)

if wlan_status != 3:
    raise RuntimeError('Wi-Fi connection failed')
else:
    print('Connected')
    status = wlan.ifconfig()
    print('ip = ' + status[0])

# wlan security codes
def wlan_security(security):
    if security == 0:
        type = 'None'
    elif security == 1:
        type = 'WEP'
    elif security == 2:
        type = 'WPA-PSK'
    elif security == 3:
        type = 'WPA2-PSK'
    elif security > 3:
        type = 'WPA/WPA2-PSK'
    return type
        
# HTTP server with reusable socket
led = machine.Pin('LED', machine.Pin.OUT)
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(addr)
s.listen(1)

print('Listening on', addr)

def dateconvert(seconds):
    datearray = time.localtime(seconds)
    strdatearray = []
    for date in datearray:
        if date<10:
            strdate="0"+str(date)
        else:
            strdate=str(date)
        strdatearray.append(strdate)
    return strdatearray[0]+"-"+strdatearray[1]+"-"+strdatearray[2]+" "+strdatearray[3]+":"+strdatearray[4]+":"+strdatearray[5]

# Cleanup wlans
def wlanout(wlans):
    output = []

    for wlan in wlans:
        ssid=wlan[0].decode('utf-8')
        mac=ubinascii.hexlify(wlan[1],':').decode('utf-8')
        channel=wlan[2]
        strength=wlan[3]
        security=wlan_security(wlan[4])
        if wlan[5]==0:
            visibility='Hidden'
        else:
            visibility='Visible'
        output.append([ssid,mac,channel,strength,security,visibility])
    return output

def buildhtml(networks):
    response = ""
    for net in networks:
        response = response + '<tr>'
        for i in range(len(net)):
            if i==6 or (i==7 and net[i]>0):
                response = response + '<td>' + dateconvert(net[i]) + '</td>'
            else:
                response = response + '<td>' + str(net[i]) + '</td>'
    response = response + '</tr>\n'
    return response
nets = []
# Listen for connections
while True:
    try:
        cl, addr = s.accept()
        print('Client connected from', addr)
        r = cl.recv(1024)
        cl.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
        wlans = wlanout(wlan.scan())
        for net in wlans:
            known=0
            for compare in nets:
                if len(nets)>0 and net[0]==compare[0] and net[1]==compare[1]:
                    known+=1
                    if len(compare)==8:
                        compare[7]=time.time()
                    else:
                        compare.append(time.time())
            if known==0:
                net.append(time.time())
                net.append(time.time())
                nets.append(net)
        if len(nets[0])>7:
            nets = sorted(nets, key=lambda x: (-x[7], -x[3]))
        html1 = """<!DOCTYPE html><html><head><title>Pico W</title><meta http-equiv="refresh" content="5"</head><body text="green" bgcolor="black"><h1>Pico W</h1>\n"""
        html2 = '<p>Active wlans: '+ str(len(wlans)) + '</p>\n<p>Total found: '+ str(len(nets)) +'</p>\n<table>\n<tr>\n<th>SSID</th><th>MAC</th><th>Channel</th><th>Strength</th><th>Security</th><th>Visibility</th><th>First seen</th><th>Last seen</th>\n'
        html3 = """</body></html>"""
        cl.send(html1+html2)
        chunk_size = 20
        for i in range(0, len(nets), chunk_size):
            cl.send(buildhtml(nets[i:i+chunk_size]))
        cl.send(html3)
        cl.close()
    except OSError as e:
        cl.close()
        print('Connection closed')