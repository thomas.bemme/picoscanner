# picoscanner
wlan scanner for pico-w

Howto:
1. Create secrets.py
```
secrets = {
    'ssid': '<your ssid>',
    'pw': '<your wlan password>'
```
2. Upload all files main.py, secrets.py
3. Execute on pico via Thonny etc
3. Navigate to ip `http://<pico-w-ip>`

Screenshot:
![picoscanner in a browser](/pico_w.jpg?raw=true)
